const express = require('express');
const multer = require('multer');
const router = express.Router();
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const Cocktail = require('../models/Cocktail');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {

    router.get('/', (req, res) => {
        if (req.query.userID) {
            Cocktail.find({user: req.query.userID}).populate('user')
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        } else {
            Cocktail.find().populate('user')
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        }
    });

    router.get('/:id', (req, res) => {
        Cocktail.find({_id: req.params.id})
            .then(results => res.send(results[0]))
            .catch(() => res.sendStatus(500));
    });

    router.post('/', [auth, upload.single('image')], async (req, res) => {
        let cocktail = req.body;

        cocktail.user = req.user._id;

        if (req.file) {
            cocktail.image = req.file.filename;
        } else {
            cocktail.image = 'noImage.png';
        }

        cocktail.ingredients = JSON.parse(cocktail.ingredients);

        const c = new Cocktail(cocktail);

        c.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    router.put('/:id', auth, async (req, res) => {
        const cocktail = await Cocktail.findOne({_id: req.params.id});

        let userVoted = false;
        let index;
        cocktail.rating.forEach((rate, key) => {
            if (req.body.user.toString() === rate.user.toString()) {
                userVoted = true;
                index = key;
            }
        });

        if (userVoted) {
            cocktail.rating[index].rate = req.body.rate;
        } else {
            cocktail.rating.push(req.body);
        }

        cocktail.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    router.delete('/:id', auth, (req, res) => {

        Cocktail.deleteOne({_id: req.params.id})
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error))
    });

    router.post(`/publish`, [auth, permit('admin')], async (req, res) => {
        const id = req.body.cocktail;

        const cocktail = await Cocktail.findOne({_id: id});
        cocktail.published = true;

        cocktail.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    router.put('/edit/:id', [auth, permit('admin'), upload.single('image')], async (req, res) => {
        let cocktail = req.body;

        cocktail.user = req.user._id;

        if (req.file) {
            cocktail.image = req.file.filename;
        } else {
            cocktail.image = 'noImage.png';
        }

        cocktail.ingredients = JSON.parse(cocktail.ingredients);

        let c = await Cocktail.findOneAndUpdate({_id: req.params.id}, cocktail);

        c.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;