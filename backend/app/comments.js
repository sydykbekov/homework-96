const express = require('express');
const router = express.Router();
const Comment = require('../models/Comment');
const auth = require('../middleware/auth');

const createRouter = () => {
    router.get('/:id', (req, res) => {
        Comment.find({cocktailID: req.params.id}).populate('user cocktailID')
            .then(comments => res.send(comments))
            .catch(error => res.status(400).send(error));
    });

    router.post('/', auth, async (req, res) => {
        const comment = req.body;

        const c = new Comment(comment);

        await c.save();

        return res.send(c);
    });

    return router;
};

module.exports = createRouter;