const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const IngredientSchema = new Schema({
    name: {
        type: String, required: true
    },
    amount: {
        type: String, required: true
    }
});

const RatingSchema = new Schema({
    user: String,
    rate: Number
});

const CocktailSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    title: {
        type: String,
        required: true
    },
    image: String,
    recipe: {
        type: String,
        required: true
    },
    published: {
        type: Boolean,
        default: false
    },
    ingredients: {
        type: [IngredientSchema]
    },
    rating: {
        type: [RatingSchema]
    }
});

const Cocktail = mongoose.model('Cocktail', CocktailSchema);

module.exports = Cocktail;