const User = require('../models/User');
const jwt = require('jsonwebtoken');
const config = require('../config');


const auth = async (req, res, next) => {
    const token = req.get('Token');

    if(!token) {
        return res.status(401).send({error: 'There is no token'});
    }

    let tokenData;

    try {
        tokenData = jwt.verify(token, config.jwt.secret);
    } catch (e) {
        return res.status(401).send({Message: 'Token incorrect'});
    }

    const user = await User.findById(tokenData.id);

    if(!user) {
        return res.status(401).send({error: 'User not found'});
    }

    req.user = user;

    next();
};

module.exports = auth;