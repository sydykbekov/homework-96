const mongoose = require('mongoose');
const config = require('./config');

const Cocktail = require('./models/Cocktail');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('users');
        await db.dropCollection('cocktails');
    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    const [Mike, Carl] = await User.create({
        username: 'Mike',
        password: '123'
    }, {
        username: 'Carl',
        password: '123',
        role: 'admin'
    });

    await Cocktail.create({
        title: 'Молочный коктейль с бананом',
        image: 'milkCocktail.jpg',
        ingredients: [{name: 'Молоко', amount: '150 мл'}, {name: 'Банан', amount: '2 штуки'}, {name: 'Сливочное мороженое', amount: '2 столовые ложки'}],
        recipe: 'Нарезать бананы, положить в миксер. Добавить к ним молоко и мороженое. Взбить до однородной массы.',
        user: Mike._id
    }, {
        title: 'Коктейль «Апероль-шприц» ',
        image: 'aperolSpritz.jpg',
        ingredients: [{name: 'Просекко', amount: '100 мл'}, {name: 'Газированная вода', amount: '50 мл'}, {name: 'Цедра апельсина', amount: 'по вкусу'}, {name: 'Аперитив апероль', amount: '50 мл'}],
        recipe: 'В бокал со льдом влить просекко, апероль и газированную воду и перемешать барной ложкой.',
        user: Carl._id
    });

    db.close();
});