const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    db: {
        url: 'mongodb://localhost:27017',
        name: 'cocktails'
    },
    jwt: {
        secret: 'some kinda very secret string',
        expires: '1h'
    },
    facebook: {
        appId: "1830613310315895",
        appSecret: "139e435e1f00e655f70f957785714f02"
    }
};