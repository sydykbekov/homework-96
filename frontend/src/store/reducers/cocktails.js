import {
    FETCH_COMMENTS_SUCCESS, FETCH_MY_COCKTAILS_SUCCESS, FETCH_COCKTAIL_SUCCESS,
    FETCH_COCKTAILS_SUCCESS
} from "../actions/actionTypes";
const initialState = {
    cocktails: [],
    cocktail: null,
    comments: [],
    myCocktails: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COCKTAILS_SUCCESS:
            return {...state, cocktails: action.cocktails};
        case FETCH_COCKTAIL_SUCCESS:
            return {...state, cocktail: action.cocktail};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, comments: action.comments};
        case FETCH_MY_COCKTAILS_SUCCESS:
            return {...state, myCocktails: action.cocktails};
        default:
            return state;
    }
};

export default reducer;