import axios from '../../axios-api';
import {
    FETCH_COCKTAILS_SUCCESS, FETCH_COCKTAIL_SUCCESS, FETCH_COMMENTS_SUCCESS, FETCH_MY_COCKTAILS_SUCCESS
} from "./actionTypes";
import {push} from "react-router-redux";
import {NotificationManager} from "react-notifications";

export const fetchCocktailsSuccess = cocktails => {
    return {type: FETCH_COCKTAILS_SUCCESS, cocktails};
};

export const fetchCocktailSuccess = cocktail => {
    return {type: FETCH_COCKTAIL_SUCCESS, cocktail};
};

export const fetchCommentsSuccess = comments => {
    return {type: FETCH_COMMENTS_SUCCESS, comments};
};

export const getMyCocktailsSuccess = cocktails => {
    return {type: FETCH_MY_COCKTAILS_SUCCESS, cocktails};
};

export const fetchCocktails = () => {
    return dispatch => {
        axios.get('cocktails').then(response => {
            dispatch(fetchCocktailsSuccess(response.data));
        })
    }
};

export const createCocktail = formData => {
    return dispatch => {
        axios.post('cocktails', formData).then(() => {
            NotificationManager.success('success', 'Your cocktail is created!');
            dispatch(push('/'));
        })
    }
};

export const fetchCocktail = cocktailID => {
    return dispatch => {
        axios.get(`cocktails/${cocktailID}`).then(response => {
            dispatch(fetchCocktailSuccess(response.data));
        })
    }
};

export const vote = (cocktailID, userID, rate) => {
    return dispatch => {
        axios.put(`cocktails/${cocktailID}`, {user: userID, rate: rate}).then(response => {
            if (response.data.error) {
                NotificationManager.error('error', response.data.error);
            } else {
                dispatch(fetchCocktail(cocktailID));
                NotificationManager.success('success', 'Your vote counted!');
            }
        });
    };
};

export const getComments = cocktailID => {
    return dispatch => {
        axios.get(`comments/${cocktailID}`).then(response => {
            dispatch(fetchCommentsSuccess(response.data));
        })
    }
};

export const addComment = comment => {
    return dispatch => {
        axios.post('comments', comment).then(() => {
            dispatch(getComments(comment.cocktailID));
        })
    };
};

export const getMyCocktails = id => {
    return dispatch => {
        axios.get(`cocktails?userID=${id}`).then(response => {
            dispatch(getMyCocktailsSuccess(response.data));
        })
    }
};

export const removeCocktail = id => {
    return dispatch => {
        axios.delete(`cocktails/${id}`).then(() => {
            dispatch(push('/'));
            NotificationManager.success('success', 'Your cocktails was removed!');
            dispatch(fetchCocktails());
        })
    }
};

export const publish = id => {
    return dispatch => {
        axios.post(`cocktails/publish`, {cocktail: id}).then(() => {
            dispatch(fetchCocktails());
        })
    }
};

export const edit = (formData, id) => {
    return dispatch => {
        axios.put(`cocktails/edit/${id}`, formData).then(() => {
            dispatch(push('/'));
        })
    }
};