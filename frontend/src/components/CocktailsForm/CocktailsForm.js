import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "react-bootstrap";
import FormElement from "../UI/Form/FormElement";
import {connect} from "react-redux";
import {createCocktail, edit} from "../../store/actions/cocktails";

class CocktailsForm extends Component {
    state = {
        title: '',
        image: '',
        ingredients: [{name: '', amount: ''}],
        recipe: ''
    };

    submitFormHandler = event => {
        event.preventDefault();

        let state = {...this.state};

        state.ingredients = JSON.stringify(state.ingredients);
        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });

        if (this.props.props && this.props.props.match.params.id) {
            this.props.edit(formData, this.props.props.match.params.id);
        } else {
            this.props.onSubmit(formData);
        }
    };

    titleChangeHandler = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    inputChangeHandler = (id, event) => {
        const ingredients = [...this.state.ingredients];
        ingredients[id][event.target.name] = event.target.value;
        this.setState({ingredients: ingredients});
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    addIngredient = () => {
        let ingredients = [...this.state.ingredients];
        ingredients.push({name: '', amount: ''});
        this.setState({ingredients: ingredients});
    };

    reduceIngredient = index => {
        const ingredients = [...this.state.ingredients];
        ingredients.splice(index, 1);
        this.setState({ingredients: ingredients});
    };

    render() {
        return (
            <Form horizontal onSubmit={this.submitFormHandler}>
                <FormElement
                    propertyName="title"
                    title="Name"
                    type="text"
                    value={this.state.title}
                    changeHandler={this.titleChangeHandler}
                    required
                />
                {this.state.ingredients.map((ingredient, key) =>
                    <Col smOffset={1} key={key}>
                        <Col sm={5} md={5}>
                            <FormElement
                                propertyName="name"
                                title={key === 0 ? 'Ingredients' : ''}
                                type="text"
                                value={ingredient.name}
                                changeHandler={(event) => this.inputChangeHandler(key, event)}
                                placeholder="Ingredient name"
                                required
                            />
                        </Col>
                        <Col sm={5} md={5}>
                            <FormElement
                                propertyName="amount"
                                type="text"
                                value={ingredient.amount}
                                changeHandler={(event) => this.inputChangeHandler(key, event)}
                                placeholder="Amount"
                                required
                            />
                        </Col>
                        {key !== 0 && <Col sm={2} md={2}>
                            <Button bsStyle="danger" onClick={() => this.reduceIngredient(key)}>Delete</Button>
                        </Col>}
                    </Col>
                )}

                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button bsStyle="success" onClick={this.addIngredient}>Add ingredient</Button>
                    </Col>
                </FormGroup>

                <FormElement
                    propertyName="recipe"
                    title="Recipe"
                    type="text"
                    value={this.state.recipe}
                    changeHandler={this.titleChangeHandler}
                    required
                />

                <FormElement
                    propertyName="image"
                    title="Image"
                    type="file"
                    changeHandler={this.fileChangeHandler}
                />

                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button bsStyle="primary" type="submit" style={{marginRight: '20px'}}>Create cocktail</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    onSubmit: formData => dispatch(createCocktail(formData)),
    edit: (formData, id) => dispatch(edit(formData, id))
});

export default connect(null, mapDispatchToProps)(CocktailsForm);
