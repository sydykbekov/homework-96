import React from 'react';
import {Route, Switch} from "react-router-dom";

import NewCocktail from "./containers/NewCocktail/NewCocktail";
import Register from "./containers/Register/Register";
import Cocktails from "./containers/Cocktails/Cocktails";
import Login from "./containers/Login/Login";
import Cocktail from './containers/Cocktail/Cocktail';
import MyCocktails from "./containers/MyCocktails/MyCocktails";
import Edit from "./containers/Edit/Edit";

const Routes = () => (
    <Switch>
        <Route path="/" exact component={Cocktails}/>
        <Route path="/new-cocktail" exact component={NewCocktail}/>
        <Route path="/register" exact component={Register}/>
        <Route path="/login" exact component={Login}/>
        <Route path="/cocktails/:id" exact component={Cocktail}/>
        <Route path="/my-cocktails" exact component={MyCocktails}/>
        <Route path="/edit/:id" exact component={Edit}/>
    </Switch>
);

export default Routes;