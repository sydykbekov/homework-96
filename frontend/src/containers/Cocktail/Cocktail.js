import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {NotificationManager} from 'react-notifications';
import {Thumbnail, Button, Badge, FormGroup, Form, Panel, Col} from 'react-bootstrap';
import FormElement from '../../components/UI/Form/FormElement';
import './Cocktail.css';
import {addComment, fetchCocktail, getComments, vote} from "../../store/actions/cocktails";

class Cocktail extends Component {
    state = {
        rates: [1, 2, 3, 4, 5],
        comment: ''
    };

    componentDidMount() {
        this.props.getCocktail(this.props.match.params.id);
        this.props.getComments(this.props.match.params.id);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    vote = (event, rate) => {
        event.preventDefault();

        if (this.props.user) {
            this.props.vote(this.props.match.params.id, this.props.user._id, rate);
        } else {
            this.props.history.push('/login');
            NotificationManager.error('Error', 'At first, you should be logged in!');
        }
    };

    submitCommentFormHandler = event => {
        event.preventDefault();

        const comment = {
            text: this.state.comment,
            user: this.props.user._id,
            cocktailID: this.props.match.params.id
        };

        this.props.addComment(comment);
    };

    render() {
        let rateAmount = 0;
        let votes;
        let rate;
        let userVoted;
        if (this.props.cocktail && this.props.cocktail.rating.length > 0) {
            this.props.cocktail.rating.forEach(rating => {
                rateAmount += rating.rate;
                if (this.props.user && rating.user === this.props.user._id) userVoted = rating.rate;
            });
            votes = this.props.cocktail.rating.length;
            rate = rateAmount / votes;
        } else {
            votes = 0;
            rate = 0;
        }

        return (
            <Fragment>
                {this.props.cocktail && <Thumbnail src={'http://localhost:8000/uploads/' + this.props.cocktail.image} style={{boxShadow: '2px 2px 10px grey', width: '500px'}}>
                    <h2>{this.props.cocktail.title}</h2><Badge>{!this.props.cocktail.published && 'На рассмотрении у модератора'}</Badge>
                    <h4>Rating: {rate} ({votes} votes)</h4>
                    <h4>Ingredients:</h4>
                    {this.props.cocktail.ingredients.map((ingredient, key) =>
                        <li key={key}>{ingredient.name} - {ingredient.amount}</li>
                    )}
                    <p><b>Recipe:</b> {this.props.cocktail.recipe}</p>
                    <p><b>Rate:</b>{this.state.rates.map(rate => <Button key={rate} onClick={(event) => this.vote(event, rate)}>{rate}</Button>)}</p>
                    {userVoted && <span>Ваша текущая оценка: {userVoted}</span>}
                </Thumbnail>}
                {this.props.user && <Panel>
                    <Panel.Body>
                        <Form horizontal onSubmit={this.submitCommentFormHandler}>
                            <FormElement
                                propertyName="comment"
                                title="Comment"
                                placeholder="comment ..."
                                autoComplete="new-comment"
                                type="text"
                                value={this.state.comment}
                                changeHandler={this.inputChangeHandler}
                                required
                            />
                            <FormGroup>
                                <Col smOffset={2} sm={10}>
                                    <Button
                                        bsStyle="primary"
                                        type="submit"
                                    >Create comment</Button>
                                </Col>
                            </FormGroup>
                        </Form>
                    </Panel.Body>
                </Panel>}
                {this.props.comments.map(comment => <Panel bsStyle="info" style={{width: '90%', margin: '10px auto'}} key={comment._id}>
                    <Panel.Heading>
                        <h4>{comment.user.username}:</h4>
                        {comment.text}
                    </Panel.Heading>
                </Panel>)}
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    cocktail: state.cocktails.cocktail,
    user: state.users.user,
    comments: state.cocktails.comments
});

const mapDispatchToProps = dispatch => ({
    getCocktail: id => dispatch(fetchCocktail(id)),
    vote: (cocktailID, userID, rate) => dispatch(vote(cocktailID, userID, rate)),
    addComment: comment => dispatch(addComment(comment)),
    getComments: id => dispatch(getComments(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Cocktail);
