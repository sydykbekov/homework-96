import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Grid, PageHeader, Row} from "react-bootstrap";
import CocktailsListItem from "../../components/CocktailsListItem/CocktailsListItem";
import {getMyCocktails, removeCocktail} from "../../store/actions/cocktails";

class MyCocktails extends Component {
    componentDidMount() {
        this.props.getMyCocktails(this.props.user._id);
    }

    render() {
        return (
            <Fragment>
                <PageHeader>
                    My cocktails
                </PageHeader>
                <Grid>
                    <Row className="show-grid">
                        {this.props.myCocktails.map(cocktail => (
                            <CocktailsListItem
                                key={cocktail._id}
                                id={cocktail._id}
                                title={cocktail.title}
                                image={cocktail.image}
                                remove={() => this.props.removeCocktail(cocktail._id)}
                            />
                        ))}
                    </Row>
                </Grid>
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    myCocktails: state.cocktails.myCocktails
});

const mapDispatchToProps = dispatch => ({
    getMyCocktails: id => dispatch(getMyCocktails(id)),
    removeCocktail: cocktailID => dispatch(removeCocktail(cocktailID))
});

export default connect(mapStateToProps, mapDispatchToProps)(MyCocktails);