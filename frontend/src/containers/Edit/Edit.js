import React, {Component, Fragment} from 'react';
import {PageHeader} from "react-bootstrap";
import CocktailsForm from "../../components/CocktailsForm/CocktailsForm";

class Edit extends Component {
    render() {
        return (
            <Fragment>
                <PageHeader>Edit cocktail</PageHeader>
                <CocktailsForm props={this.props}/>
            </Fragment>
        );
    }
}

export default Edit;