import React, {Component, Fragment} from 'react';
import {PageHeader} from "react-bootstrap";
import CocktailsForm from "../../components/CocktailsForm/CocktailsForm";

class NewCocktail extends Component {
    render() {
        return (
            <Fragment>
                <PageHeader>New cocktail</PageHeader>
                <CocktailsForm/>
            </Fragment>
        );
    }
}

export default NewCocktail;