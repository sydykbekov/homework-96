import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader, Grid, Row} from "react-bootstrap";
import {Link} from "react-router-dom";

import CocktailsListItem from '../../components/CocktailsListItem/CocktailsListItem';
import {fetchCocktails, publish, removeCocktail} from "../../store/actions/cocktails";

class Cocktails extends Component {
    componentDidMount() {
        this.props.getCocktails();
    }

    editCocktail = id => {
        this.props.history.push(`edit/${id}`);
    };

    render() {
        return (
            <Fragment>
                <PageHeader>
                    Cocktails
                    {this.props.user &&
                    <Link to="/new-cocktail">
                        <Button bsStyle="primary" className="pull-right">
                            Add cocktail
                        </Button>
                    </Link>
                    }
                </PageHeader>
                <Grid>
                    <Row className="show-grid">
                        {this.props.user && this.props.user.role === 'admin' ? this.props.cocktails.map(cocktail => (
                            <CocktailsListItem
                                key={cocktail._id}
                                id={cocktail._id}
                                title={cocktail.title}
                                image={cocktail.image}
                                publish={!cocktail.published ? () => this.props.publish(cocktail._id) : null}
                                remove={() => this.props.removeCocktail(cocktail._id)}
                                edit={() => this.editCocktail(cocktail._id)}
                            />
                        )) : this.props.cocktails.map(cocktail => (
                            cocktail.published &&
                            <CocktailsListItem
                                key={cocktail._id}
                                id={cocktail._id}
                                title={cocktail.title}
                                image={cocktail.image}
                            />
                        ))}
                    </Row>
                </Grid>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user,
        cocktails: state.cocktails.cocktails
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getCocktails: () => dispatch(fetchCocktails()),
        removeCocktail: cocktailID => dispatch(removeCocktail(cocktailID)),
        publish: cocktailID => dispatch(publish(cocktailID))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Cocktails);